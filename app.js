#!/usr/bin/env node
import { Command } from 'commander'
import 'dotenv/config'
import { initConfigServices } from "./src/initConfig/InitConfig.js";
import { DataBaseService } from "./src/dataBase/DataBase.js";
import { UpConfigServices } from "./src/upConfig/UpConfig.js";
import { CONFIG } from "./src/common/constants.js";
import { SetupServices } from "./src/setup/Setup.js";
import { TranslateServices } from "./src/translate/Translate.js";

const commander = new Command;

// TODO:  use it in instalition
// TODO:  ▹▹▹▹▸ Installation in progress... ☕

commander
  .version('1.0.0')
  .description('Shiptify CLI.');

////////////////////////////////////////////
///// init
////////////////////////////////////////////
commander
  .command('init')
  .description('Выполняет настройку проекта, запрашивая у пользователя данные и сохраняя их в соответствующих файлах конфигурации.')
  .action(async () => {
    await initConfigServices.setup()
  });

////////////////////////////////////////////
///// config
////////////////////////////////////////////
// TODO: Переисеновать мб сгрупировать под 1 командой
commander
  .command('config [command]')
  .alias('co')
  .description(`Создание конфигов`)
  .action(async action => {
    await UpConfigServices.action(action)
  })

commander
  .command('config all')
  .description(`Создание local.json относительно init.json По всем проектам в папке ${CONFIG.INIT_PATH}`);

////////////////////////////////////////////
///// up
////////////////////////////////////////////
// TODO: Разбить на шаги установку что бы можно было отдельно выполнить старт докера
commander
  .command('up [command]')
  .description('Устанавливаем зависимости')
  .action(async action => {
    await SetupServices.action(action)
  })

commander
  .command('up setup')
  .description('Устанавливаем зависимости, скачиваем проекты, создаем папку для инициализации, устанавливаем local.json в скачаных проектах. После перезагрузки запустите up base');

commander
  .command('up base')
  .description('Поднимаем докер и накатываем бд');

////////////////////////////////////////////
///// data-base
////////////////////////////////////////////
commander
  .command('data-base [command]')
  .alias('db')
  .description('Управление базой данных')
  .action(async action => {
    await DataBaseService.action(action)
  })

commander
  .command('data-base load')
  .description('Загрузить из бэкапа');

commander
  .command('data-base delete')
  .description('Удалить базу');

commander
  .command('data-base update')
  .description('Обновить базу данных с использованием бэкапа включает в себя db delete и db load');

commander
  .command('data-base create-backup')
  .description('Создать бэкап базы данных');

////////////////////////////////////////////
///// translate
////////////////////////////////////////////
commander
  .command('translate [folder]')
  .alias('tr')
  .description('Поиск всех непереведеных файлов')
  .action(async folder => {
    await TranslateServices.run(folder)
  })

// TODO: Добавить переводчик что бы строкой принимал папку которую перевести translate|tr main <folder>

commander.parse(process.argv);
