import { dirname } from "path";
import { spawn } from "child_process";

class Global {
  constructor() {
  }

  /**
   * Получает имя директории текущего модуля.
   * @param filename
   * @returns {string} Полный путь к директории текущего файла.
   */
  getDirname(filename) {
    return dirname(filename)
  }

  /**
   * Выполняет указанный скрипт в оболочке bash.
   * @param {string} scriptPath Путь к исполняемому скрипту.
   * @returns {Promise<void>} Промис, который разрешается при успешном выполнении скрипта, или отклоняется с ошибкой, если скрипт завершается с ненулевым кодом.
   */
  runCommand(scriptPath) {
    return new Promise((resolve, reject) => {
      const process = spawn('bash', [scriptPath], { stdio: 'inherit' });

      process.on('close', (code) => {
        if (code !== 0) {
          reject(new Error(`Скрипт ${ scriptPath } завершился с кодом ${ code }`));
        } else {
          resolve();
        }
      });
    });
  }

}

export const GlobalServices = new Global();
