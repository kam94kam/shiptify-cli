class DateService {
  createFileNameWithTimeByInput(text) {
    // Разбиваем входящую строку на части
    const parts = text.split('.');
    // Извлекаем имя файла и формат
    const name = parts[0]; // Берем первую часть до точки как имя
    const format = parts[parts.length - 1]; // Берем последнюю часть как формат

    // Получаем текущую дату и время
    const now = new Date();

    // Форматируем дату и время в строку в формате "ГГГГ_ММ_ДД_ЧЧ_ММ_СС"
    const formattedDateTime = `${now.getFullYear()}_${String(now.getMonth() + 1).padStart(2, '0')}_${String(now.getDate()).padStart(2, '0')}_${String(now.getHours()).padStart(2, '0')}_${String(now.getMinutes()).padStart(2, '0')}_${String(now.getSeconds()).padStart(2, '0')}`;

    // Создаем и возвращаем название файла
    return `${name}_${formattedDateTime}.${format}`;
  }
}

// Пример использования
export const DateServices = new DateService();

