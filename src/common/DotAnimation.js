class DotAnimation {
  constructor() {
    this.interval = null;
    this.count = 0;
    this.frames = [
      `
,__  
 (o>
/)_)
" "\n`,
      `
,__  
 (o< | Backup is in progress |
/)_)\\
" "\n`,
      `
,__  
 (o>
/)_)\\
" "\n`,
      `
,__  
 (->
/)_)
" "\n`
    ];
  }

  start() {
    if (this.interval) {
      return; // Анимация уже запущена
    }

    this.interval = setInterval(() => {
      process.stdout.write('\r' + '.'.repeat(this.count % 4) + ' '.repeat(3 - this.count % 4));
      this.count++;
    }, 500);
  }

  stop() {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
      process.stdout.write('\r   \r'); // Очистить строку анимации
    }
  }

  birdStart() {
    if (this.interval) {
      return; // Анимация уже запущена
    }

    process.stdout.write('\n'); // Переход на новую строку перед началом анимации
    this.currentLine = 2; // Предполагается, что анимация начнется со второй строки


    this.interval = setInterval(() => {
      process.stdout.write('\x1Bc'); // Очистка экрана и сброс
      process.stdout.write('\x1B[0;0H'); // Перемещение курсора в начало экрана

      const frame = this.frames[this.count % this.frames.length];
      process.stdout.write(frame);

      this.count++;
    }, 500);
  }
}

const b = `
     ,__
      (o>
     /)_)
     " "
`

export const AnimationServices = new DotAnimation();
