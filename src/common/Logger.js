export class LoggerService {
  static log(text) {
    console.log(text);
  }

  static green(text) {
    console.log('\x1b[32m%s\x1b[0m', text); // Зеленый текст
  }

  static warn(text){
    console.log('\x1b[33m%s\x1b[0m', text); // Желтый текст
  }

  static error(text) {
    console.log('\x1b[31m%s\x1b[0m', text); // Красный текст
  }
}
