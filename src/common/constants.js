import path from "path";
import fs from "fs";
import * as dotenv from "dotenv";
import { fileURLToPath } from "url";
import { GlobalServices } from "./Global.js";

export function loadEnvVariables() {
  const dirname = GlobalServices.getDirname(fileURLToPath(import.meta.url))
  const envPath = path.resolve(dirname, '../../.env');
  // Проверяем, существует ли файл .env
  if (!fs.existsSync(envPath)) {
    throw new Error('Определите переменные выполните init-config. Приложение остановлено.');
  }

  // Загружаем переменные окружения из файла .env
  const result = dotenv.config({ path: envPath });

  if (result.error) {
    throw result.error;
  }

  return result.parsed; // Возвращает объект с переменными окружения из .env
}

const env = loadEnvVariables();

const CONFIG = {
  USER_CONFIG_FILE_NAME: 'user-config.json',
  SHIPTIFY_INIT_FOLDER: `shiptify-init`,
  SHIPTIFY_INIT_PATH: `${env.INIT_PATH}/${env.SHIPTIFY_INIT_FOLDER}`,
  DESTINATION_FILE: 'dump.sql',
  CONTAINER_NAME: 'shiptify-postgres',
  POSTGRES_USER: env.POSTGRES_USER || 'postgres',
  POSTGRES_DB: env.POSTGRES_DB || 'shiptify_dev',
  INIT_PATH: env.INIT_PATH,
  CONFIG_FOLDER: env.CONFIG_FOLDER || 'config',
  CONFIG_FILE_NAME: env.CONFIG_FILE_NAME || 'init.json',
  DATA_BASE_FOLDER: env.DATA_BASE_FOLDER || 'data-base',

  T_INPUT_FOLDER_PATH: `${env.INIT_PATH}/${env.TRANSLATE_INPUT_FOLDER_PATH}`,
  T_JSON_FILE_PATH: `${env.INIT_PATH}/${env.TRANSLATE_JSON_FILE_PATH}`,
  T_OUTPUT_JSON_FILE_PATH: `${env.INIT_PATH}/${env.SHIPTIFY_INIT_FOLDER}/${env.TRANSLATE_OUTPUT_JSON_FILE_PATH}`,
};

export { CONFIG }
