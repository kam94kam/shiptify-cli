import { HtmlJsParserServices } from "./HtmlJsParser.js";
import { FileFinderServices } from "./FileFinder.js";
import fs from "fs";
import { CONFIG } from "../common/constants.js";
import { LoggerService } from "../common/Logger.js";
import path from 'path';

class BaseService {
  constructor() {
    if (new.target === BaseService) {
      throw new Error("Cannot instantiate an abstract class.");
    }
    if (this.requiredMethod === BaseService.prototype.requiredMethod) {
      throw new Error("You have to implement the method requiredMethod!");
    }
    if (this.anotherRequiredMethod === BaseService.prototype.anotherRequiredMethod) {
      throw new Error("You have to implement the method anotherRequiredMethod!");
    }
  }

  requiredMethod() {
    throw new Error("You have to implement the method requiredMethod!");
  }

  anotherRequiredMethod() {
    throw new Error("You have to implement the method anotherRequiredMethod!");
  }

  commonMethod() {
    console.log("This is a common method.");
  }
}


class Translate {
  /**
   * Создает экземпляр класса Translate.
   * @param {Object} HtmlJsParserServices - Сервисы для парсинга HTML и JS файлов.
   * @param {Object} FileFinderServices - Сервисы для поиска файлов.
   * @param {Object} LoggerService - Сервисы для логирования.
   * @param {Object} CONFIG - Конфигурационные параметры.
   * @param {string} CONFIG.T_INPUT_FOLDER_PATH - Путь к входной папке.
   * @param {string} CONFIG.T_JSON_FILE_PATH - Путь к JSON файлу.
   * @param {string} CONFIG.T_OUTPUT_JSON_FILE_PATH - Путь к выходному JSON файлу.
   */
  constructor(
    HtmlJsParserServices,
    FileFinderServices,
    LoggerService,
    CONFIG
  ) {
    this.HtmlJsParserServices = HtmlJsParserServices;
    this.FileFinderServices = FileFinderServices;
    this.loggerService = LoggerService;
    this.config = CONFIG;

    this.htmlKeys = [];
    this.htmlVar = [];
    this.htmlNgBind = [];
    this.htmlOnlyString = [];
    this.jsKeys = [];
  }

  /**
   * Инициализирует необходимые пути и проверяет их существование.
   * @throws {Error} Если путь к входной папке или JSON файлу не существует.
   */
  init() {
    const {
      T_INPUT_FOLDER_PATH,
      T_JSON_FILE_PATH,
      T_OUTPUT_JSON_FILE_PATH,
    } = this.config;

    this.inputFolderPath = T_INPUT_FOLDER_PATH;
    this.jsonFilePath = T_JSON_FILE_PATH;
    this.outputJsonFilePath = T_OUTPUT_JSON_FILE_PATH;

    // Ensure paths exist before proceeding
    if (!fs.existsSync(this.inputFolderPath)) {
      const msg = `Input folder path does not exist: ${ this.inputFolderPath }`

      this.loggerService.error(msg)

      throw new Error(msg);
    }

    if (!fs.existsSync(this.jsonFilePath)) {
      const msg = `JSON file path does not exist: ${ this.jsonFilePath }`

      this.loggerService.error(msg)

      throw new Error(msg);
    }

    this.startJsonObject = JSON.parse(fs.readFileSync(this.jsonFilePath, 'utf8'));
  }

  /**
   * Обрабатывает найденный HTML файл, извлекая ключи, переменные и другие данные.
   * @param {string} filePath - Путь к HTML файлу.
   */
  findHTMLCallback(filePath) {
    const {
      keys,
      variable,
      ngBind,
      onlyString
    } = this.HtmlJsParserServices.parseHtmlFile(filePath, this.startJsonObject);
    this.htmlKeys = [...this.htmlKeys, ...keys];
    this.htmlVar = [...this.htmlVar, ...variable];
    this.htmlNgBind = [...this.htmlNgBind, ...ngBind];
    this.htmlOnlyString = [...this.htmlOnlyString, ...onlyString];
  }

  /**
   * Обрабатывает найденный JS файл, извлекая уникальные ключи.
   * @param {string} filePath - Путь к JS файлу.
   */
  findJSCallback(filePath) {
    const parseUniqKeys = this.HtmlJsParserServices.parseJsFile(filePath, this.startJsonObject);
    this.jsKeys = [...this.jsKeys, ...parseUniqKeys];
  }

  /**
   * Преобразует массив строк в объект JSON.
   * @param {Array} arr - Массив строк.
   * @returns {Object} Преобразованный объект JSON.
   */
  objectToJson(arr) {
    return [...new Set(arr)].reduce((acc, key) => {
      acc[ key ] = key;
      return acc;
    }, {});
  }

  /**
   * Группирует и записывает данные в JSON файл.
   */
  groupAndWriteJson() {
    const groupObject = {
      html: this.objectToJson(this.htmlKeys),
      htmlVariable: this.objectToJson(this.htmlVar),
      htmlNgBind: this.objectToJson(this.htmlNgBind),
      htmlOnlyString: this.objectToJson(this.htmlOnlyString),
      js: this.objectToJson(this.jsKeys),
    };

    fs.writeFileSync(this.outputJsonFilePath, JSON.stringify(groupObject, null, 4), 'utf8');


    const outputPath = path.resolve(this.outputJsonFilePath);

    this.loggerService.green(`Переводы созданы. Вы можете найти их по следующему пути: file://${outputPath}`);
  }

  /**
   * Запускает процесс поиска и обработки файлов.
   * @param {string} [path=''] - Дополнительный путь к папке.
   */
  run(path = '') {
    this.init()

    const dir = `${ this.inputFolderPath }/${ path }`

    if (!fs.existsSync(dir)) {
      const msg = `Указанный путь не существует: ${dir}`;

      LoggerService.error(msg);

      return;
    }

    this.FileFinderServices.findHtmlFiles(dir, filePath => this.findHTMLCallback(filePath));
    this.FileFinderServices.findJsFiles(dir, filePath => this.findJSCallback(filePath));
    this.groupAndWriteJson();
  }
}

export const TranslateServices = new Translate(
  HtmlJsParserServices,
  FileFinderServices,
  LoggerService,
  CONFIG,
)
