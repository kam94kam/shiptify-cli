import fs from "fs";
import cheerio from "cheerio";
import { checkCaseType } from "./helper.js";

/**
 * Класс для парсинга HTML и JS файлов и извлечения ключей для перевода.
 */
class HtmlJsParser {
  /**
   * Создает экземпляр класса HtmlJsParser.
   */
  constructor() {
    this.HTMLkeys = [];
    this.JSkeys = [];
    this.variable = [];
    this.ngBind = [];
    this.onlyString = [];
  }

  /**
   * Извлекает текст без кавычек из HTML контента.
   *
   * @param {string} htmlContent - HTML контент для парсинга.
   * @returns {string[]} Массив текстов, найденных в HTML.
   */
  parseTextWithoutQuotes(htmlContent) {
    const regex = />([^<{]+)</g;
    let texts = [];
    let match;

    while ((match = regex.exec(htmlContent)) !== null) {
      const text = match[1].trim();
      if (!/{{.*}}/.test(text) && text.length && !/ng-|[-+]|--->|\.|«|»|\/&gt;|→|\/|,|—|\||&gt;|:|;|%/.test(text)) {
        texts.push(text);
      }
    }
    return texts;
  }

  /**
   * Парсит HTML файл и извлекает ключи для перевода.
   *
   * @param {string} filePath - Путь к HTML файлу.
   * @param {Object} jsonContent - JSON контент для проверки существующих ключей.
   * @returns {Object} Объект, содержащий массивы найденных ключей, переменных и строк.
   */
  parseHtmlFile(filePath, jsonContent) {
    let htmlContent = fs.readFileSync(filePath, 'utf8');
    const $ = cheerio.load(htmlContent);

    $('body').each((i, el) => {
      const bodyHtml = $(el).html();
      const regexCombined = /(\{\{(?:'([^']*)'|"([^"]*)"|([^'"\|]*))\s*\|\s*translate\s*\}\})|(ng-bind\s*=\s*['"]([^'"]*)\s*\|\s*translate['"])/g;
      let match;
      const parseTexts = this.parseTextWithoutQuotes(bodyHtml);

      if (parseTexts.length) this.onlyString = [...this.onlyString, ...parseTexts];

      while ((match = regexCombined.exec(bodyHtml)) !== null) {
        const newKey = match[2] || match[3] || match[4] || match[6];

        if (!jsonContent.hasOwnProperty(newKey)) {
          // Использование checkCaseType для фильтрации ключей
          if (checkCaseType(newKey.trim())) {
            if (match[1]) {
              this.variable.push(newKey);
            } else {
              this.ngBind.push(newKey);
            }
          } else if (!newKey.includes('.')) {
            this.HTMLkeys.push(newKey);
          }
        }
      }
    });

    fs.writeFileSync(filePath, htmlContent, 'utf8');

    return { keys: this.HTMLkeys, variable: this.variable, ngBind: this.ngBind, onlyString: this.onlyString };
  }

  /**
   * Парсит JS файл и извлекает ключи для перевода.
   *
   * @param {string} filePath - Путь к JS файлу.
   * @param {Object} jsonContent - JSON контент для проверки существующих ключей.
   * @returns {string[]} Массив найденных ключей.
   */
  parseJsFile(filePath, jsonContent) {
    const jsContent = fs.readFileSync(filePath, 'utf8');
    const regexTranslate = /translate\s*\(\s*['"]([^'"]+)['"]\s*\)/g;
    let match;

    while ((match = regexTranslate.exec(jsContent)) !== null) {
      const key = match[1];

      if (!jsonContent.hasOwnProperty(key) && !key.includes('.')) {
        this.JSkeys.push(key);
      }
    }

    return this.JSkeys;
  }
}

export const HtmlJsParserServices = new HtmlJsParser();
