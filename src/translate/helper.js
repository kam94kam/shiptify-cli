export const CAMEL_CASE = 1;
export const SNAkE_CASE = 2;
export const NO_CASE = 0;

export const checkCaseType = str => {
  const isCamelCase = /^[a-z]+([A-Z][a-z]*)*$/.test(str);
  const isSnakeCase = /^[a-z]+(_[a-z]+)*$/.test(str);

  if (isCamelCase && /[A-Z]/.test(str)) {
    return CAMEL_CASE;
  } else if (isSnakeCase && /_/.test(str)) {
    return SNAkE_CASE;
  } else {
    return NO_CASE;
  }
}
