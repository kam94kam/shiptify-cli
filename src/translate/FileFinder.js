import { join, extname } from "path";
import fs from "fs";

/**
 * Класс для поиска файлов.
 */
class FileFinder {
  /**
   * Создает экземпляр класса FileFinder.
   */
  constructor() {
  }

  /**
   * Ищет HTML файлы в указанной директории и вызывает callback для каждого найденного файла.
   *
   * @param {string} dir - Директория для поиска файлов.
   * @param {function} callback - Функция обратного вызова, вызываемая для каждого найденного файла.
   */
  findHtmlFiles(dir, callback) {
    this.findFiles('.html', dir, callback);
  }

  /**
   * Ищет JS файлы в указанной директории и вызывает callback для каждого найденного файла.
   *
   * @param {string} dir - Директория для поиска файлов.
   * @param {function} callback - Функция обратного вызова, вызываемая для каждого найденного файла.
   */
  findJsFiles(dir, callback) {
    this.findFiles('.js', dir, callback);
  }

  /**
   * Общий метод для поиска файлов с указанным расширением в заданной директории.
   *
   * @param {string} ext - Расширение файла для поиска.
   * @param {string} dir - Директория для поиска файлов.
   * @param {function} callback - Функция обратного вызова, вызываемая для каждого найденного файла.
   */
  findFiles(ext, dir, callback) {
    fs.readdirSync(dir).forEach(file => {
      const fullPath = join(dir, file);

      if (fs.lstatSync(fullPath).isDirectory()) {
        this.findFiles(ext, fullPath, callback);
      } else if (extname(fullPath) === ext) {
        callback(fullPath);
      }
    });
  }
}

export const FileFinderServices = new FileFinder();
