import { exec } from 'child_process';
import chalk from 'chalk'
import { CONFIG } from "../common/constants.js";

/**
 * Запускает процесс миграции проекта, включая обновление кода из Git, выполнение миграций и возврат к предыдущему состоянию Git ветки.
 * Использует настройки из CONFIG для определения пути миграции.
 *
 * Процесс:
 * 1. Переходит в директорию миграций.
 * 2. Проверяет текущую ветку. Если не `develop`, переключается на неё.
 * 3. Выполняет `git stash`, `git pull`, `npm run migrate` для выполнения миграций.
 * 4. Возвращает Git к исходной ветке и применяет сташ, если это было необходимо.
 *
 * @returns {Promise<string>} Промис, который разрешается с сообщением о успешном выполнении миграций или отклоняется с ошибкой.
 */
export function runMigrationProcess() {
    return new Promise((resolve, reject) => {
        const cdToMigration = `cd "${CONFIG.INIT_PATH}/migrations"`
        const changeDirectoryAndCheckBranch = `${cdToMigration} && git rev-parse --abbrev-ref HEAD`;

        exec(changeDirectoryAndCheckBranch, (err, currentBranch) => {
            if (err) {
                reject(`Ошибка при проверке текущей ветки: \n${err}`);
                return;
            }

            let commands = `
                source ~/.nvm/nvm.sh &&
                git stash &&
                git pull &&
                npm run migrate &&
                git checkout - &&
                git stash pop &&
                cd -
            `;

            if (currentBranch.trim() !== 'develop') {
                commands = `git checkout develop && ${commands}`;
            } else {
                // Если мы уже на ветке 'develop', пропускаем 'git checkout develop'
                commands = `git pull && ${commands}`;
            }

            const fullCommand = `${cdToMigration} && ${commands}`;

            exec(fullCommand, { shell: '/bin/bash' }, (error, stdout, stderr) => {
                if (error) {
                    reject(`Ошибка: ${error}`);

                    return;
                }

                if (stderr && !stderr.includes("Already on 'develop'")) {
                    reject(`Ошибка stderr: \n${stderr}`);

                    return;
                }

                resolve(`${chalk.green('Результат')}: \n${stdout}`);
            });
        });
    });
}
