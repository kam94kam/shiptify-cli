import { join } from "path";
import { runMigrationProcess } from "./runMigration.js";
import { AnimationServices } from "../common/DotAnimation.js";
import { LoggerService } from "../common/Logger.js";
import { GlobalServices } from "../common/Global.js";
import { fileURLToPath } from "url";

export class DataBase {

  constructor(GlobalServices, LoggerService) {
    this.Global = GlobalServices;
    this.loggerService = LoggerService;

    const dirname = this.Global.getDirname(fileURLToPath(import.meta.url))

    this.scripts = {
      installDataBase: join(dirname, 'bash/copy_and_create.sh'),
      deleteDB: join(dirname, 'bash/delete_db.sh'),
      renewDbAndUpdatePassword: join(dirname, 'bash/renew_db_and_update_password.sh'),
      madeBackup: join(dirname, 'bash/backup_db.sh'),
    };
  }

  async runMigration() {
    // AnimationServices.start();
    try {
      const result = await runMigrationProcess();

      this.loggerService.log(result);
      this.loggerService.green(`\nОбновление завершено успешно.`)
    } catch (error) {
      this.loggerService.error('\nОшибка при выполнении обновления:')
      this.loggerService.error(error)
    } finally {
      // AnimationServices.stop();
    }
  }

  /**
   * Создает новый бэкап текущей базы данных.
   * @returns {Promise<void>} Промис без возвращаемого значения, который разрешается после успешного создания бэкапа.
   */
  async createBackup() {
    AnimationServices.birdStart();
    try {
     await this.Global.runCommand(this.scripts.madeBackup)
    } catch (error) {
      this.loggerService.error(error);
    } finally {
      AnimationServices.stop();
    }
  }

  /**
   * Загружает и восстанавливает базу данных из бэкапа.
   * @returns {Promise<void>} Промис без возвращаемого значения, который разрешается после успешного выполнения команды.
   */
  async loadBackup() {
    try {
      await this.Global.runCommand(this.scripts.installDataBase)
    } catch (error) {
      this.loggerService.error(error);
    }
  }

  /**
   * Удаляет существующий бэкап базы данных.
   * @returns {Promise<void>} Промис без возвращаемого значения, который разрешается после успешного удаления бэкапа.
   */
  async deleteBackup() {
    try {
      await this.Global.runCommand(this.scripts.deleteDB)
    } catch (error) {
      this.loggerService.error(error);
    }
  }

  /**
   * Обновляет базу данных, применяя новый бэкап. Этот процесс включает удаление текущей базы данных и восстановление из бэкапа.
   * @returns {Promise<void>} Промис без возвращаемого значения, который разрешается после успешного обновления базы данных.
   */
  async updateDatabaseWithBackup() {
    try {
      await this.Global.runCommand(this.scripts.renewDbAndUpdatePassword)
    } catch (error) {
      this.loggerService.error(error);
    }
  }

  /**
   * Выполняет указанную команду управления базой данных.
   * Поддерживает команды для загрузки бэкапа, удаления бэкапа, обновления базы данных с использованием бэкапа и создания нового бэкапа.
   * @param {string} command Команда для выполнения. Доступные команды: 'load', 'delete', 'update', 'create-backup'.
   * @returns {Promise<void>} Promise, который разрешается после выполнения команды или отклоняется при возникновении ошибки.
   */
  async action(command) {
    switch (command) {
      case 'load':
        // TODO: добавить сброс пароля
        await this.loadBackup();
        break;
      case 'delete':
        await this.deleteBackup();
        break;
      case 'update':
        await this.updateDatabaseWithBackup();
        break;
      case 'create-backup':
        await this.createBackup();
        break;
      default:
        this.loggerService.log(`Неизвестная команда: ${ command }`);
        this.loggerService.log('Доступные команды для "db":');
        this.loggerService.log('load - Загрузить и восстановить из бэкапа');
        this.loggerService.log('delete - Удалить бэкап');
        this.loggerService.log('update - Обновить базу данных с использованием бэкапа');
        this.loggerService.log('create-backup - Создать бэкап базы данных');
        break;
    }
  }

  // TODO: SMS notifer db add the same method md create new service
}

export const DataBaseService = new DataBase(GlobalServices, LoggerService)
