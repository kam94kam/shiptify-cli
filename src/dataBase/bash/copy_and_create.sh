#!/bin/bash
source $(dirname "$0")/../../../.env

DUMP_FILE_PATH="${INIT_PATH}/${SHIPTIFY_INIT_FOLDER}/data-base/${SOURCE_FILE}"

# Проверка на наличие SOURCE_FILE и файла
if [ -z "${SOURCE_FILE}" ] || [ ! -f "${INIT_PATH}/${SHIPTIFY_INIT_FOLDER}/data-base/${SOURCE_FILE}" ]; then
    echo -e "\033[33mSOURCE_FILE не задан или файл не найден. Ищу самый свежий дамп...\033[0m"
    # Ищем самый свежий SQL-файл, содержащий 'dump' в названии
    DUMP_FILE_PATH=$(ls -t "${INIT_PATH}/${SHIPTIFY_INIT_FOLDER}/data-base/"*dump 2>/dev/null | head -n 1)
    if [ -z "${DUMP_FILE_PATH}" ]; then
        echo -e "\033[31mНе найден ни один dump-файл.\033[0m"
        exit 1
    else
        echo -e "\033[32mНайден файл дампа: ${DUMP_FILE_PATH}\033[0m"
    fi
else
    DUMP_FILE_PATH="${INIT_PATH}/${SHIPTIFY_INIT_FOLDER}/data-base/${SOURCE_FILE}"
fi

## Копирование дампа БД в контейнер
docker cp ${DUMP_FILE_PATH} ${CONTAINER_NAME}:/${DISTANCE_FILE}
if [ $? -eq 0 ]; then
    echo -e "\033[32mБаза данных успешно скопирована.\033[0m"
else
    echo -e "\033[31mОшибка при копировании базы данных.\033[0m"
    exit 1
fi

## Проверка наличия БД и её создание в случае отсутствия
DB_EXISTS=$(docker exec ${CONTAINER_NAME} bash -c "PGPASSWORD=${POSTGRES_PASSWORD} psql -U ${POSTGRES_USER} -tAc \"SELECT 1 FROM pg_database WHERE datname='${POSTGRES_DB}'\"")
if [ "$DB_EXISTS" != "1" ]; then
    echo -e "\033[33mБаза данных ${POSTGRES_DB} не найдена. Создаю...\033[0m"
    docker exec -it ${CONTAINER_NAME} bash -c "PGPASSWORD=${POSTGRES_PASSWORD} psql -U ${POSTGRES_USER} -c 'CREATE DATABASE \"${POSTGRES_DB}\";'"
    if [ $? -eq 0 ]; then
        echo -e "\033[32mБаза данных ${POSTGRES_DB} успешно создана.\033[0m"
    else
        echo -e "\033[31mОшибка при создании базы данных ${POSTGRES_DB}.\033[0m"
        exit 1
    fi
else
    echo -e "\033[32mБаза данных ${POSTGRES_DB} уже существует.\033[0m"
fi

## Запуск команды внутри контейнера для восстановления БД
docker exec -it ${CONTAINER_NAME} bash -c "PGPASSWORD=${POSTGRES_PASSWORD} psql -U ${POSTGRES_USER} ${POSTGRES_DB} < /${DISTANCE_FILE}"
if [ $? -eq 0 ]; then
    echo -e "\033[32mБаза данных успешно установлена.\033[0m"
else
    echo -e "\033[31mОшибка при установке базы данных.\033[0m"
    exit 1
fi
