#!/bin/bash
# Настройка переменных
SCRIPT_DIR=$(dirname "$0")

source "$SCRIPT_DIR/../../../.env"

## Вызов существующих скриптов для удаления и установки БД
bash "$SCRIPT_DIR/delete_db.sh"
if [ $? -ne 0 ]; then
    echo -e "\033[31mОшибка при удалении базы данных.\033[0m"
    exit 1
fi

bash "$SCRIPT_DIR/copy_and_create.sh"
if [ $? -ne 0 ]; then
    echo -e "\033[31mОшибка при создании базы данных.\033[0m"
    exit 1
fi

## Обновление пароля пользователей
docker exec -it ${CONTAINER_NAME} bash -c "PGPASSWORD=${POSTGRES_PASSWORD} psql -U ${POSTGRES_USER} ${POSTGRES_DB} -c \"UPDATE users SET password = '${NEW_PASSWORD}'\""
if [ $? -eq 0 ]; then
    echo -e "\033[32mПароль пользователей успешно обновлен.\033[0m"
else
    echo -e "\033[31mОшибка при обновлении пароля пользователей.\033[0m"
    exit 1
fi

echo -e "\033[32mПроцесс обновления базы данных и паролей пользователей выполнен успешно.\033[0m"
