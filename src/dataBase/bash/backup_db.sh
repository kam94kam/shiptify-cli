#!/bin/bash
source $(dirname "$0")/../../../.env

# Получение текущей даты в формате ГГГГММДД_ЧЧММСС
CURRENT_DATE=$(date +"%Y%m%d_%H%M%S")
# Определение пути назначения с использованием переменной окружения и текущей даты
DUMP_FILE="${INIT_PATH}/${SHIPTIFY_INIT_FOLDER}/${CURRENT_DATE}_dump.sql"

DB_EXISTS=$(docker exec ${CONTAINER_NAME} bash -c "PGPASSWORD=${POSTGRES_PASSWORD} psql -U ${POSTGRES_USER} -tAc \"SELECT 1 FROM pg_database WHERE datname='${POSTGRES_DB}';\"")

if [ "$DB_EXISTS" == "1" ]; then
  ## Запуск команды внутри контейнера для создания дампа БД
  docker exec -t ${CONTAINER_NAME} pg_dump -U ${POSTGRES_USER} ${POSTGRES_DB} > ${DUMP_FILE}

  echo -e "\033[32mДамп базы данных успешно создан в файле ${DUMP_FILE}.\033[0m"
else
    echo -e "\033[33mБаза данных ${POSTGRES_DB} не найдена.\033[0m"
fi
