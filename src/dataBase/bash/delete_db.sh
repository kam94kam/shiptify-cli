#!/bin/bash
source $(dirname "$0")/../../../.env

# Проверка наличия БД
DB_EXISTS=$(docker exec ${CONTAINER_NAME} bash -c "PGPASSWORD=${POSTGRES_PASSWORD} psql -U ${POSTGRES_USER} -tAc \"SELECT 1 FROM pg_database WHERE datname='${POSTGRES_DB}';\"")

if [ "$DB_EXISTS" == "1" ]; then
    ## Запуск команды внутри контейнера для удаления БД, если она существует
    docker exec -it ${CONTAINER_NAME} bash -c "PGPASSWORD=${POSTGRES_PASSWORD} psql -U ${POSTGRES_USER} -c 'DROP DATABASE IF EXISTS ${POSTGRES_DB};'"
    echo -e "\033[32mБаза данных ${POSTGRES_DB} успешно удалена.\033[0m"
else
    echo -e "\033[33mБаза данных ${POSTGRES_DB} не найдена. Удаление не требуется.\033[0m"
fi
