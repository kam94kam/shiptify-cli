import { LoggerService } from "../common/Logger.js";
import { promises as fs } from "fs";
import path from "path";
import inquirer from "inquirer";
import { CONFIG } from "../common/constants.js";
import chalk from "chalk";

class InitConfig {
  constructor(LoggerService) {
    this.loggerService = LoggerService;
  }

  /**
   * Запрашивает у пользователя конфигурационные данные для PostgreSQL и путь инициализации, предлагая разумные значения по умолчанию.
   * @returns {Promise<Object>} Возвращает объект конфигурации с POSTGRES_USER, POSTGRES_DB и INIT_PATH.
   */
  async promptForConfig() {
    const defaultPath = process.cwd();

    const answers = await inquirer.prompt([
      {
        type: 'input',
        name: 'POSTGRES_USER',
        message: `Введите имя пользователя PostgreSQL ${chalk.italic(`(Default: ${CONFIG.POSTGRES_USER})`)}:`,
      },
      {
        type: 'input',
        name: 'POSTGRES_DB',
        message: `Введите название базы данных PostgreSQL ${chalk.italic(`(Default: ${CONFIG.POSTGRES_DB})`)}:`,
      },
      {
        type: 'input',
        name: 'INIT_PATH',
        // Этот путь должен браться относительно папки в которой вызывается команда из терминала
        message: `Введите путь для инициализации ${chalk.italic(`(Default: ${defaultPath})`)}:`,
      }
    ]);

    return Object.keys(answers).reduce((acc, key) => {
      if (!answers[key].length) {
        acc[key] = CONFIG[key];
      } else {
        acc[key] = answers[key];
      }

      return acc
    }, {})
  }

  /**
   * Обновляет или добавляет ключи конфигурации в файл .env.
   * Эта функция специально обрабатывает ключи POSTGRES_USER, POSTGRES_DB и INIT_PATH,
   * добавляя или обновляя их значения в соответствии с предоставленным объектом config.
   * Если ключ уже существует, его значение будет обновлено. Если ключ отсутствует, он будет добавлен.
   *
   * @param {Object} config Объект конфигурации с ключами и значениями для обновления.
   *                        Должен содержать POSTGRES_USER, POSTGRES_DB, и INIT_PATH.
   * @returns {Promise<void>} Промис без возвращаемого значения, который разрешается после успешной записи в файл.
   * @throws {Error} В случае ошибки чтения/записи файла выбрасывается исключение.
   */
  async writeConfigToEnvFile(config) {
    const envPath = path.resolve(process.cwd(), '.env');

    try {
      // Попытка доступа к файлу для проверки его существования
      await fs.access(envPath);

      // Чтение текущего содержимого файла .env
      const currentEnv = await fs.readFile(envPath, 'utf8');
      const envVariables = currentEnv.split('\n').reduce((acc, line) => {
        const [key, value] = line.split('=');
        if (key && value) {
          acc[key.trim()] = value.trim();
        }
        return acc;
      }, {});

      // Обновление или добавление новых значений
      // Обновление ключей конфигурации
      Object.keys(config).forEach((key) => {
        envVariables[key] = config[key];
      });

      // Формирование нового содержимого .env файла
      const newEnvContent = Object.entries(envVariables)
        .map(([key, value]) => `${key}=${value}`)
        .join('\n');

      // Запись в файл
      await fs.writeFile(envPath, newEnvContent);
    } catch (error) {
      // Если файл .env не существует, создаем его с новыми значениями
      if (error.code === 'ENOENT') {
        const envContent = `
          POSTGRES_USER=${config.POSTGRES_USER}
          POSTGRES_DB=${config.POSTGRES_DB}
          INIT_PATH=${config.INIT_PATH}
        `.trim();
        await fs.writeFile(envPath, envContent);
      } else {
        throw error;
      }
    }
  }

  /**
   * Записывает данные конфигурации в JSON-файл. Файл будет создан или перезаписан.
   * @param {Object} config Объект конфигурации для записи.
   * @returns {Promise<void>} Промис без возвращаемого значения, разрешающийся после успешной записи в файл.
   * @throws {Error} Выбрасывает ошибку при неудачной попытке записи файла.
   */
  async writeConfigToJsonFile(config) {
    const configContent = JSON.stringify(config, null, 2); // Форматирование для удобочитаемости
    try {
      await fs.writeFile(path.resolve(process.cwd(), CONFIG.USER_CONFIG_FILE_NAME), configContent);
    } catch (error) {
      throw error;
    }
  }

  /**
   * Выполняет настройку проекта, запрашивая у пользователя данные и сохраняя их в соответствующих файлах конфигурации.
   * @returns {Promise<void>} Промис без возвращаемого значения, разрешающийся после успешной настройки.
   * @throws {Error} Выбрасывает ошибку, если процесс настройки не удался.
   */
  async setup() {
    try {
      const config = await this.promptForConfig();
      await this.writeConfigToEnvFile(config)

      this.loggerService.green('Конфигурация успешно сохранена');
    } catch (error) {
      this.loggerService.error('Ошибка при записи:')
      this.loggerService.error(error)
      throw error;
    }
  }
}

export const initConfigServices = new InitConfig(LoggerService)
