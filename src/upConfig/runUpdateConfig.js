import fs from 'fs';
import path from 'path';
import { CONFIG } from "../common/constants.js";
import { LoggerService } from "../common/Logger.js";

const { INIT_PATH, SHIPTIFY_INIT_FOLDER } = CONFIG;
export const SHIPTIFY_UPDATE_CONFIG = `${SHIPTIFY_INIT_FOLDER}/config`;
const DEFAULT_CONFIG_FILE = `${SHIPTIFY_UPDATE_CONFIG}/init.json`;
const configFolder = 'config';
const inputFile = 'default.json';
const outputFile = 'local.json';

/**
 * Рекурсивно сливает базовую конфигурацию с конфигурацией переопределения, не мутируя исходные объекты.
 *
 * @param {Object|Array} baseConfig - Базовый объект или массив конфигураций.
 * @param {Object|Array} overrideConfig - Объект или массив конфигураций, которые должны переопределить базовую конфигурацию.
 * @param {Array} [missingKeys=[]] - Опциональный массив, который будет заполнен ключами, присутствующими в baseConfig, но отсутствующими в overrideConfig.
 * @param {string} [prefix=''] - Строка префикса, используемая при добавлении путей к недостающим ключам, представляет текущий путь в рекурсивном проходе.
 * @returns {Object|Array} - Новый объект или массив, представляющий слияние baseConfig и overrideConfig.
 */
function mergeConfigs(baseConfig, overrideConfig, missingKeys = [], prefix = '') {
  let result = Array.isArray(baseConfig) ? [] : {};

  for (const key in baseConfig) {
    if (baseConfig.hasOwnProperty(key)) {
      const fullKey = prefix + key;
      if (!overrideConfig.hasOwnProperty(key)) {
        missingKeys.push(fullKey); // Ключ отсутствует в overrideConfig, добавляем в список
      }

      if (typeof baseConfig[key] === 'object' && baseConfig[key] !== null && !Array.isArray(baseConfig[key])) {
        // Если значение ключа является объектом, делаем рекурсивный вызов
        result[key] = mergeConfigs(baseConfig[key], overrideConfig[key] || {}, missingKeys, fullKey + '.');
      } else {
        // Иначе берем значение из overrideConfig, если оно есть, или из baseConfig, если его нет
        result[key] = overrideConfig.hasOwnProperty(key) ? overrideConfig[key] : baseConfig[key];
      }
    }
  }

  return result;
}

/**
 * Запускает процесс обновления конфигурации, считывая начальные настройки и объединяя их
 * с пользовательскими конфигурациями в каждом подпроекте. Создает локальный файл конфигурации
 * на основе этих данных.
 *
 * @param {boolean} [logs=false] - Флаг для включения логирования в процессе.
 * @returns {Promise<string>} - Обещание, которое разрешается с сообщением о статусе генерации.
 *
 * @example
 * runUpdateConfigProcess(true).then(statusMessage => {
 *   console.log(statusMessage);
 * });
 */
export async function runUpdateConfigProcess(logs = false) {
  const initConfigPath = path.join(INIT_PATH, DEFAULT_CONFIG_FILE);
  const initConfig = JSON.parse(fs.readFileSync(initConfigPath, 'utf8'));

  // Обход директорий проекта
  fs.readdirSync(INIT_PATH, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory() && dirent.name !== SHIPTIFY_INIT_FOLDER) // Исключаем папку init
    .forEach(dirent => {
      const projectDirPath = path.join(INIT_PATH, dirent.name);
      const configDirPath = path.join(projectDirPath, configFolder);
      const defaultConfigPath = path.join(configDirPath, inputFile);
      const missingKeys = []; // Сбор недостающих ключей для каждого проекта

      if (fs.existsSync(defaultConfigPath)) {
        const defaultConfig = JSON.parse(fs.readFileSync(defaultConfigPath, 'utf8'));
        const localConfig = mergeConfigs(defaultConfig, initConfig, missingKeys); // Слияние конфигов без мутации

        if (missingKeys.length > 0 && logs) {
          LoggerService.warn(`В проекте ${dirent.name} отсутствуют ключи:`);
          LoggerService.log(`${missingKeys.join(', ')}`);
        }

        const localConfigPath = path.join(configDirPath, outputFile);
        fs.writeFileSync(localConfigPath, JSON.stringify(localConfig, null, 2), 'utf8');
      }
    });

  return 'Генерация local.json завершена.';
}
