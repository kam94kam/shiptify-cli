import { LoggerService } from "../common/Logger.js";
import { runUpdateConfigProcess, SHIPTIFY_UPDATE_CONFIG } from "./runUpdateConfig.js";
import { CONFIG } from "../common/constants.js";

export class UpConfig {
  constructor(LoggerService) {
    this.loggerService = LoggerService;
  }

  async runUpdateConfig() {
    try {
      const result = await runUpdateConfigProcess()

      this.loggerService.green(`\n${ result }`);
    } catch (error) {
      this.loggerService.error(error);
    }
  }

  async action(command) {
    switch (command) {
      case 'all':
        await this.runUpdateConfig();
        break;
      default:
        this.loggerService.log(`Неизвестная команда: ${ command }`);
        this.loggerService.log('Доступные команды для "config":');
        this.loggerService.log(`all - Создание local.json относительно init.json По всем проектам в папке ${CONFIG.INIT_PATH}`);
        break;
    }
  }
}

export const UpConfigServices = new UpConfig(LoggerService);
