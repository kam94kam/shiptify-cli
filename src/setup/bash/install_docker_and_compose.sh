#!/bin/bash

# Проверка на операционную систему
if [[ "$OSTYPE" == "darwin"* ]]; then
  echo "Вы используете macOS"

  # Установка Docker и Docker Compose на macOS
  echo "Установка Docker и Docker Compose через Homebrew..."
  brew update
  brew install --cask docker
  brew install docker-compose

  echo "Установка завершена."
  docker --version
  docker-compose --version

elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
  echo "Вы используете Linux"

  # Установка Docker и Docker Compose на Ubuntu
  echo "Установка Docker..."
  sudo apt-get update
  sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  sudo apt-get update
  sudo apt-get install -y docker-ce

  # Добавление текущего пользователя в группу docker
  sudo usermod -aG docker $USER

  # Установка Docker Compose
  echo "Установка Docker Compose..."
  COMPOSE_VERSION=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4)
  sudo curl -L "https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose

  echo "Установка завершена."
  docker --version
  docker-compose --version

else
  echo "Неподдерживаемая операционная система: $OSTYPE"
  exit 1
fi
