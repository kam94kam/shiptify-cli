# Загрузка переменных окружения
source $(dirname "$0")/../../../.env
# Задать разрешение на выполнение всей текущей директории
# Не очень безопастно
PS4='LINENO:'
chmod -R +x "$PWD"

# Задайте массив с URL-ами Git-репозиториев
REPO_URLS=(
    "git@gitlab.com:shiptify/apps/main-app/frontend.git"
    "git@gitlab.com:shiptify/apps/main-app/backend.git"
    "git@gitlab.com:shiptify/apps/back-office.git"
    "git@gitlab.com:shiptify/apps/db/migrations.git"
    "git@gitlab.com:shiptify/mini-apps.git"
    "git@gitlab.com:shiptify/packages/translations.git"
    "git@gitlab.com:shiptify/emailing.git"
    "git@gitlab.com:shiptify/apps/public-api.git"
    "git@gitlab.com:shiptify/public-api-docs.git"
    "git@gitlab.com:shiptify/apps/microfrontend/auth.git"
    "git@gitlab.com:shiptify/apps/attachments/generate.git"
)

# Переход в родительский каталог Shiptify
cd "${INIT_PATH}" || exit # Если переход не удался, выходим из скрипта с ошибкой

## Используйте цикл для клонирования каждого репозитория
for URL in "${REPO_URLS[@]}"; do
    # Извлеките имя репозитория из URL
    REPO_NAME=$(basename "${URL}" .git)

    # Проверьте наличие каталога репозитория
    if [ -d "${REPO_NAME}" ]; then
        echo -e "\033[33mРепозиторий ${REPO_NAME} уже скачан, пропуск клонирования.\033[0m"
    else
        # Если репозиторий не скачан, выполните клонирование
        git clone "${URL}"
        echo -e "\033[32mРепозиторий ${REPO_NAME} успешно склонирован.\033[0m"
    fi
done
