#!/bin/bash

# Установка nodemon глобально
echo "Установка Nodemon..."
if npm install -g nodemon; then
  echo "Nodemon установлен:"
  nodemon --version
else
  echo "Ошибка при установке Nodemon"
  exit 1
fi
