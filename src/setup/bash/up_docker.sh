## Поднимите контейнеры Docker из docker-compose.yml
docker-compose up -d

if [ $? -eq 0 ]; then
    echo -e "\033[32mDocker успешно поднят.\033[0m"
else
    echo -e "\033[31mОшибка при запуске Docker Compose.\033[0m"
    echo -e "\033[33mВозможно не установлен докер или недостаточно прав.\033[0m"
    exit 1
fi

docker ps
