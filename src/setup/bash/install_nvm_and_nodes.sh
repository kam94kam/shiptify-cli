#!/bin/bash
# Установка последней версии NVM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash

# Инициализация NVM в текущей сессии
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

# Ждем завершения инициализации NVM
sleep 2

# Устанавливаем нужные версии Node.js
VERSIONS="10 12 14 16 18 20"
for VERSION in $VERSIONS; do
  nvm install $VERSION
done

echo "Установка версий Node.js завершена:"
nvm list
