import path from "path";
import { spawn } from "child_process";
import { LoggerService } from "../common/Logger.js";

import { fileURLToPath } from 'url';
import { dirname, join } from 'path';
import { CONFIG } from "../common/constants.js";
import { promises as fs } from 'fs';
import { UpConfigServices } from "../upConfig/UpConfig.js";
import inquirer from "inquirer";
import { DataBaseService } from "../dataBase/DataBase.js";
import { GlobalServices } from "../common/Global.js";

// Получаем путь к текущему файлу и директории
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);


class Setup {
  constructor(
    UpConfigServices,
    DataBaseService,
    GlobalServices,
    LoggerService,
  ) {
    this.upConfigServices = UpConfigServices;
    this.DataBaseService = DataBaseService;
    this.Global = GlobalServices;
    this.loggerService = LoggerService;

    const dirname = this.Global.getDirname(fileURLToPath(import.meta.url))

    this.scripts = {
      installNvmAndNodes: path.join(dirname, 'bash/install_nvm_and_nodes.sh'),
      installDockerAndCompose: path.join(dirname, 'bash/install_docker_and_compose.sh'),
      installNodemon: path.join(dirname, 'bash/install_nodemon.sh'),
      installingProjects: path.join(dirname, 'bash/installing_projects.sh'),

      upDocker: path.join(dirname, 'bash/up_docker.sh'),
    };
  }

  /**
   * Устанавливает NVM и Node.js.
   * @returns {Promise<void>}
   */
  async installNvmAndNodes() {
    await this.Global.runCommand(this.scripts.installNvmAndNodes);
    this.loggerService.green('Установка NVM и Node.js завершена.');
  }

  /**
   * Устанавливает Docker и Docker Compose.
   * @returns {Promise<void>}
   */
  async installDockerAndCompose() {
    await this.Global.runCommand(this.scripts.installDockerAndCompose);
    this.loggerService.green('Установка Docker и Docker Compose завершена.');
  }

  /**
   * Устанавливает Nodemon.
   * @returns {Promise<void>}
   */
  async installNodemon() {
    await this.Global.runCommand(this.scripts.installNodemon);
    this.loggerService.green('Установка Nodemon завершена.');
  }

  /**
   * Выполняет скачивание проектов.
   * @returns {Promise<void>}
   */
  async installingProjects() {
    await this.Global.runCommand(this.scripts.installingProjects);
    this.loggerService.green('Скачка проектов завершена.');
  }

  /**
   * Поднимает Docker.
   * @returns {Promise<void>}
   */
  async upDocker() {
    await this.Global.runCommand(this.scripts.upDocker);
    this.loggerService.green('Docker поднят.');
  }

  /**
   * Устанавливает все зависимости.
   * @returns {Promise<void>}
   */
  async installingDependencies() {
    await this.installNvmAndNodes();
    await this.installDockerAndCompose();
    await this.installNodemon();
    this.loggerService.green('Все зависимости выполнены успешно.');
  }

  /**
   * Создает начальную структуру для проекта.
   * @returns {Promise<void>}
   */
  async createInitStructure() {
    try {

      // Определяем пути к директориям и файлам
      const baseDir = CONFIG.SHIPTIFY_INIT_PATH;
      const configDir = join(baseDir, 'config');
      const databaseDir = join(baseDir, 'data-base');
      const initFile = join(configDir, 'init.json');

      // Создаем директории
      await fs.mkdir(configDir, { recursive: true });
      await fs.mkdir(databaseDir, { recursive: true });

      // // Создаем init.json с пустым объектом, если файл не существует
      try {
        await fs.access(initFile);
        this.loggerService.warn('Файл init.json уже существует.');
      } catch {
        await fs.writeFile(initFile, '{}');
        this.loggerService.log('Файл init.json создан.');
      }

      this.loggerService.green('Структура shiptify-init успешно создана.');
    } catch (error) {
      throw Error(`[createShiptifyInitStructure] Ошибка при создании структуры: \n${ error }`)
    }
  }

  /**
   * Запрашивает у пользователя добавление конфигурационных файлов.
   * @returns {Promise<void>}
   */
  async askAboutTheConfigurationFiles() {
    const baseDir = CONFIG.SHIPTIFY_INIT_PATH;
    const configDir = join(baseDir, 'config');
    const databaseDir = join(baseDir, 'data-base');

    await inquirer.prompt([
      {
        type: 'confirm',
        name: 'continue',
        message: `Пожалуйста, перед продолжением добавьте: \ndump.sql в ${ databaseDir } \ninit.json в ${ configDir } \nГотовы продолжить?`,
        default: false
      }
    ]).then(answers => {
      if (!answers.continue) {
        this.loggerService.error('Установка прервана пользователем.');
        this.loggerService.warn('Пожалуйста, добавьте необходимые файлы и запустите скрипт снова.');
        process.exit(1); // Выход из приложения, если пользователь не готов продолжить
      }
    });

  }

  /**
   * Запрашивает у пользователя установку зависимостей.
   * @returns {Promise<boolean>} Возвращает `true`, если пользователь согласен.
   */
  async askAboutInstallingDependencies() {
    return await inquirer.prompt([
      {
        type: 'confirm',
        name: 'continue',
        message: `Установить зависимости?`,
        default: true
      }
    ]).then(answers => {
      if (!answers.continue) this.loggerService.log('Без зависимостей.');

      return answers.continue
    });
  }

  /**
   * Запрашивает у пользователя поднять Docker.
   * @returns {Promise<boolean>} Возвращает `true`, если пользователь согласен.
   */
  async askAboutDockerComposeUp() {
    return await inquirer.prompt([
      {
        type: 'confirm',
        name: 'continue',
        message: `Поднять Docker?`,
        default: false
      }
    ]).then(answers => {
      if (!answers.continue) {
        this.loggerService.log('Docker не поднят.');
      }

      return answers.continue
    });
  }

  /** Запрашивает у пользователя выполнение установки dump DB.
   * @returns {Promise<boolean>} Возвращает `true`, если пользователь согласен.
   */
  async askAboutDataBaseDump() {
    return await inquirer.prompt([
      {
        type: 'confirm',
        name: 'continue',
        message: `Выполнить установку dump DB??`,
        default: false
      }
    ]).then(answers => {
      if (!answers.continue) {
        this.loggerService.log('Не выполнять установку dump DB.');
      }

      return answers.continue
    });
  }

  // async dockerComposeUp() {
  //   try {
  //     this.loggerService.log('Запуск Docker Compose...');
  //     const dockerComposePath = '/home/igor/Desktop/projects/shiptify-cli';
  //
  //     const processDocker = spawn('docker-compose', ['up', '-d'], {
  //       cwd: process.cwd(), // Устанавливаем рабочую директорию
  //       stdio: 'inherit', // Перенаправляем stdio для отображения в консоли
  //       shell: true, // Используем shell для интерпретации команды
  //     });
  //
  //     return new Promise((resolve, reject) => {
  //       processDocker.on('close', (code) => {
  //         if (code === 0) {
  //           this.loggerService.green('Docker Compose успешно запущен.');
  //           resolve();
  //         } else {
  //           this.loggerService.error('Ошибка при запуске Docker Compose.');
  //           reject(new Error(`Process exited with code ${code}`));
  //         }
  //       });
  //     });
  //   } catch (error) {
  //     this.loggerService.error(`[dockerComposeUp] Ошибка: \n${error.message}`);
  //     throw error; // Перебрасываем ошибку дальше
  //   }
  // }

  /**
   * Поднимает Docker и, если требуется, выполняет установку базы данных.
   * @returns {Promise<void>}
   */
  async upDockerAndDataBase() {
    // Поднимаем докер
    if (await this.askAboutDockerComposeUp()) {
      await this.upDocker()
    }

    // Накат дампа || если есть
    if (await this.askAboutDataBaseDump()) {
      await this.DataBaseService.loadBackup()
    }
  }

  /**
   * Запускает процесс установки: установка зависимостей, создание структуры, скачивание проектов и т.д.
   * @returns {Promise<void>}
   */
  async setup() {
    try {
      // Спросить про зависимости
      if (await this.askAboutInstallingDependencies()) {
        // Зависимости
        await this.installingDependencies()
      }

      // TODO: СОЗДАВАТЬ папку в приложении generate/tmp/generated-files, она в гит игноре без нее не запускаетсяя приложение
      // /home/ihar/Desktop/work/shiptify/generate/resources to src coopy

      // Создание структуры
      await this.createInitStructure()
      // Скачка проектов с гитаn
      await this.installingProjects()
      // Просьба добавить дамп и файл локал джейсон
      await this.askAboutTheConfigurationFiles()
      // Создание локал джейсона в проектах || если есть
      await this.upConfigServices.runUpdateConfig()
      // Указываем на то что надо выйти из системы
      this.loggerService.error(`Установка завершена, пожалуйста перезагрузите систему и продолжите установку`);
    } catch (error) {
      this.loggerService.error(`[setup] Ошибка при установке: \n${ error.message }`);
    }
  }

  /**
   * Выполняет действие в зависимости от указанной команды.
   * Доступные команды:
   * - 'setup': Запускает процесс установки, включая установку зависимостей, скачивание проектов и создание начальной структуры.
   * - 'up': Поднимает Docker и, при необходимости, выполняет установку базы данных.
   * @param {string} command Команда для выполнения. Должна быть одной из предопределённых строк ('setup' или 'up').
   * @returns {Promise<void>}
   */
  async action(command) {
    switch (command) {
      case 'setup':
        await this.setup();
        break;
      case 'base':
        await this.upDockerAndDataBase();
        break;
      default:
        this.loggerService.log(`Неизвестная команда: ${ command }`);
        this.loggerService.log('Доступные команды для "up":');
        this.loggerService.log('setup - Устанавливаем зависимости, скачиваем проекты, создаем папку для инициализации, устанавливаем local.json в скачаных проектах');
        this.loggerService.log('base - Поднимаем докер и накатываем бд');
        break;
    }
  }
}

export const SetupServices = new Setup(
  UpConfigServices,
  DataBaseService,
  GlobalServices,
  LoggerService,
);
